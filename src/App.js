import React, { Component } from 'react';
import moment from 'moment'
import './style.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment()
    }
    this.clock = this.clock.bind(this);
  }

  clock() {
    this.setState(() => {
      return { date: moment() }
    })
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.clock(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerId)
  }

  render() {
    return (
      <div>
        <p className="clock">
          {moment(this.state.date).format('DD/MM/YYYY hh:mm:ss')}</p>
      </div>
    );
  }
}
